# Add some local paths
if [ -d $HOME/local/bin ]; then PATH="$PATH:$HOME/local/bin"; fi
if [ -d $HOME/local/lib ]; then LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HOME/local/lib"; fi

# if in homebrew on osx, use gnu coreutils, and put /usr/local first
if [[ $(uname) == "Darwin" && -x /usr/local/bin/brew ]]; then
    PATH="/usr/local/opt/coreutils/libexec/gnubin:/usr/local/bin:$PATH"
    MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
    DYLD_FALLBACK_LIBRARY_PATH+=":/usr/lib"

    # Also PS1 is not what I want here.
    PS1='\h:\w \u\$ '

    # Have to install git completion manually too
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
        . $(brew --prefix)/etc/bash_completion
    fi
fi

if test -e '`which pyenv`'; then
    eval "$(pyenv init -)"
fi

export SCREENDIR=$HOME/.screen
export PATH MANPATH

export EMAIL='Tom Whittock <tom.whittock@gmail.com>'
export EDITOR=vim
export PAGER=less
export LESSCHARSET='utf-8'
export LESS='-i -w  -z-4 -g -e -M -X -F -R -P%t?f%f \
:stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'

# LESS man page colors (makes Man pages more readable).
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

export LIBVIRT_DEFAULT_URI="qemu:///system"

stty werase undef
stty start undef
stty stop undef

shopt -s cmdhist lithist histappend no_empty_cmd_completion
shopt -s cdspell cdable_vars
shopt -s checkhash
shopt -s checkwinsize
shopt -s sourcepath
shopt -s histappend histreedit histverify
shopt -s extglob

# Disable options:
shopt -u mailwarn
unset MAILCHECK        # Don't want my shell to warn me of incoming mail.

export HISTSIZE=10000
export HISTCONTROL=ignoredups

if [[ "$TERM" == "screen" ]]; then
    export PS1='\[\ekbash (\w)\e\\\]'"$PS1"
    export PROMPT_COMMAND='echo -ne "\ek\e\\"'
fi


alias ls='ls --color=auto -h'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
if [[ $- == *i* && "$TERM_PROGRAM" != "vscode" ]]
then
    if test -e /usr/bin/screen && test -z "$STY"; then
        exec /usr/bin/screen -D -R
    fi

    if [[ -z "$SSH_AUTH_SOCK" ]]; then
        eval `ssh-agent`
    fi
fi

