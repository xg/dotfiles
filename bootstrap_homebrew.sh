#!/bin/bash

# Homebrew Formulae
# https://github.com/Homebrew/homebrew
declare -a HOMEBREW_FORMULAE=(
    "bash"
    "bash-completion"
    "python"
    "macvim --override-system-vim --HEAD"
    "cmake"
    "coreutils"
    "cscope"
    "findutils"
    "gawk"
    "gettext"
    "git"
    "gmp"
    "gnu-getopt"
    "gnu-indent"
    "gnu-sed"
    "gnu-tar"
    "gnutls"
    "libtasn1"
    "mercurial"
    "nettle"
    "p11-kit"
    "pkg-config"
    "qt"
    "readline"
    "xz"
    "automake"
    "libtool"
)

# Homebrew Alternate Casks
# https://github.com/caskroom/homebrew-versions
declare -a HOMEBREW_ALTERNATE_CASKS=(
)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

install_applications() {

    local i="", tmp=""

    # XCode Command Line Tools
    if [ $(xcode-select -p &> /dev/null; printf $?) -ne 0 ]; then
        xcode-select --install &> /dev/null

        # Wait until the XCode Command Line Tools are installed
        while [ $(xcode-select -p &> /dev/null; printf $?) -ne 0 ]; do
            sleep 5
        done
    fi

    # Homebrew
    brew update
    brew upgrade
    brew cleanup

    # Homebrew formulae
    for i in ${!HOMEBREW_FORMULAE[*]}; do
        tmp="${HOMEBREW_FORMULAE[$i]}"
        [ $(brew list "$tmp" &> /dev/null; printf $?) -eq 0 ] \
            && printf "$tmp\n" \
            || brew install $tmp
    done

    printf "\n"

}

install_applications

