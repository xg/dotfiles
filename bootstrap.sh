#!/bin/bash

abspath() {
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

mkdir $HOME/.screen && chmod 700 $HOME/.screen # needed to ensure the socket directory is writable by this user
dotfiles=$(dirname $0)
for f in $(find $dotfiles -maxdepth 1 -type f -not \( -name $(basename $0) -o -name "Brewfile" \)); do
    echo "Linking $f .$(basename $f)"
    ln -sf $(abspath $f) $HOME/.$(basename $f)
done

if [[ $(uname) == "Darwin" ]]; then

    if [[ ! -x /usr/local/bin/brew ]]; then
        ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
    fi

    if [[ -x /usr/local/bin/brew ]]; then
        $dotfiles/bootstrap_homebrew.sh
    fi
fi
